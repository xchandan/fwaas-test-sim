import os
from optparse import OptionParser
from mininet.net import Mininet
from mininet.node import OVSBridge
from mininet.topo import Topo, SingleSwitchTopo
from mininet.cli import CLI
from mininet.clean import cleanup
from neutron.agent.common import ovs_lib
from firewall import OVSFirewallDriver as fwgDriver
from neutron.agent.linux.openvswitch_firewall.firewall import OVSFirewallDriver as sgDriver


class MininetTopo(Topo):

    def build(self, *args, **kwargs):
        switch = self.addSwitch(kwargs.get('bridge_name', 's1'))
        for hc in range(0, kwargs.get('host_count', 2)):
            host = self.addHost(kwargs.get('host_name','h') + str(hc+1))
            self.addLink(host, switch)


class SimulationTopo(object):
    def __init__(self, bridge_name, **kwargs):
        self.topo = MininetTopo(bridge_name=bridge_name, host_count=2)
        self.net = Mininet(topo=self.topo, switch=OVSBridge, controller=None)
        self.bridge_name = bridge_name
        self.bridge_port_map = {}

    def start_topo(self):
        self.net.start()

    def prepare_bridge(self):
        os.system("ovs-ofctl del-flows %s table=0" % self.bridge_name)
        os.system("ovs-ofctl add-flow %s 'table=0 actions=resubmit(,60)'" % self.bridge_name)
        for host in self.net.hosts:
            host_ports = []
            for intf in host.intfs.values():
                pair = [intf.link.intf1.name, intf.link.intf2.name]
                br_port = pair[0] if pair[1] == intf.name else pair[1]
                host_ports.append(br_port)
            self.bridge_port_map[host.name] = host_ports

    def run_process(self, cmdstr, host_name=None):
        for host in self.net.hosts:
            if not host_name:
                host.cmd(cmdstr + '&')
            elif host_name == host.name:
                host.cmd(cmdstr + '&')
                return

    def get_ports(self, sg_name='sg1'):
        ports = []
        for host in self.net.hosts:
            host_ports = []
            for intf in host.intfs.values():
                pair = [intf.link.intf1.name, intf.link.intf2.name]
                br_port = pair[0] if pair[1] == intf.name else pair[1]
                ports.append({'security_groups': [sg_name,],
                              'device': br_port, 'lvlan': 10,
                              'allowed_address_pairs':
                     [{'ip_address': intf.ip, 'mac_address': intf.mac}]})
                os.system('ovs-vsctl set interface %(name)s external-ids:iface-id=%(name)s external-ids:iface-status=active external-ids:attached-mac=%(mac)s' %
                    {'name': br_port, 'mac': intf.mac})
                os.system('ovs-vsctl set port %(name)s other_config:tag=10' % {'name': br_port})
        return ports


class SimCLI(CLI):
    def do_rule_add(self):
        pass

    def do_rules_del(self):
        pass

    def do_show(self):
        pass

    def do_apply_policy(self):
        pass


class FWGManager(object):
    def __init__(self, name, br_name, rules=[], ports=[], enable_sg=False):
        self.name = name
        self.ingress_rules = rules.get('ingress_rules', [])
        self.egress_rules = rules.get('egress_rules', [])
        self.ports = ports
        self.br_name = br_name
        self.enable_sg = enable_sg
        self.init_br = ovs_lib.OVSBridge(br_name=self.br_name)

    def init_driver(self):
        self.drv = fwgDriver(self.init_br)

    def add_egress_rule(self, rule):
        self.ingress_rules.append(rule)

    def add_ingress_rule(self, rule):
        self.egress_rules.append(rule)

    def del_ingress_rule(self, rule):
        self.ingress_rule.pop(rule)

    def del_egress_rule(self, rule):
        self.ingress_rule.pop(rule)

    def list_rules(self, rule):
        return {'ingress_rules': self.ingress_rules, 'egress_rules': self.egress_rules}

    def set_ports(self, ports):
        self.ports += ports

    def unset_port(self, port):
        self.ports.pop(port)

    def apply_policy(self):
        firewall_group = {'id': self.name,
            'egress_rule_list': self.egress_rules,
            'ingress_rule_list': self.ingress_rules}

        self.drv.create_firewall_group(self.ports, firewall_group)


class SGManager(object):
    def __init__(self, name, br_name, rules=[], ports=[]):
        self.name = name
        self.rules = rules
        self.ports = ports
        self.br_name = br_name
        self.init_br = ovs_lib.OVSBridge(br_name=self.br_name)

    def init_driver(self):
        self.drv = sgDriver(self.init_br)

    def add_rule(self, rule):
        self.rules.append(rule)

    def list_rule(self):
        return {'rules': self.rules}

    def del_rule(self, rule):
        self.rules.pop(rule)
        
    def set_ports(self, ports):
        self.ports += ports

    def unset_port(self, port):
        self.ports.pop(port)

    def apply_policy(self):
        self.drv.update_security_group_rules(self.name, self.rules)
        for port in self.ports:
            self.drv.update_port_filter(port)


def main():
    fwg_rules = {
        'ingress_rules': [
            {'position': '1',
             'protocol': 'icmp',
             'ip_version': 4,
             'enabled': True,
             'action': 'allow',
             'id': 'fw-rule1'},
             {'position': '2',
             'protocol': 'tcp',
             'ip_version': 4,
             'port_range_min': 8000,
             'port_range_max': 8000,
             'enabled': True,
             'action': 'allow',
             'id': 'fw-rule2'},
        ],
        'egress_rules': [
            {'position': '1',
             'protocol': 'icmp',
             'ip_version': 4,
             'enabled': True,
             'action': 'allow',
             'id': 'fw-rule3'},
             {'position': '2',
             'protocol': 'tcp',
             'ip_version': 4,
             'port_range_min': 8000,
             'port_range_max': 8000,
             'enabled': True,
             'action': 'allow',
             'id': 'fw-rule4'},
        ]
    }

    sg_rules = [
        {'position': '1',
         'protocol': 'icmp',
         'ip_version': 4,
         'enabled': True,
         'ethertype': 'IPv4',
         'direction': 'ingress',
         'action': 'allow',
         'id': 'fw-rule1'},
        {'position': '2',
         'protocol': 'icmp',
         'ip_version': 4,
         'enabled': True,
         'ethertype': 'IPv4',
         'direction': 'egress',
         'action': 'allow',
         'id': 'fw-rule2'},
         {'position': '3',
         'protocol': 'tcp',
         'ip_version': 4,
         'port_range_min': 8000,
         'port_range_max': 8000,
         'enabled': True,
         'ethertype': 'IPv4',
         'direction': 'ingress',
         'action': 'allow',
         'id': 'fw-rule3'},
         {'position': '4',
         'protocol': 'tcp',
         'ip_version': 4,
         'port_range_min': 8000,
         'port_range_max': 8000,
         'enabled': True,
         'ethertype': 'IPv4',
         'direction': 'egress',
         'action': 'allow',
         'id': 'fw-rule4'}
    ]

    parser = OptionParser('Start a 2 node 1 switch topology with specified firewall driver')
    parser.add_option('--firewall', action='store',
            choices=['SG', 'FWaaS', 'both', 'none'], default='SG',
            help='choose a firewall driver SG|FWaaS|both|none default=SG')
    parser.add_option('--bridgename', action='store', default='test_br1',
            help='Bridge name to use with simulation')
    opts, args = parser.parse_args()
    br_name = opts.bridgename
    topo = SimulationTopo(bridge_name=br_name)
    topo.run_process('echo hello|nc -l 8000')
    topo.start_topo()
    topo.prepare_bridge()

    if opts.firewall == 'SG':
        sec_mgr = SGManager('SG1', br_name, rules=sg_rules, ports=topo.get_ports('SG1'))
        sec_mgr.init_driver()
        sec_mgr.apply_policy()
    elif opts.firewall == 'FWaaS':
        sec_mgr = FWGManager('FWG1', br_name, rules=fwg_rules, ports=topo.get_ports())
        sec_mgr.init_driver()
        sec_mgr.apply_policy()
    elif opts.firewall == 'both':
        sec_mgr = FWGManager('FWG1', br_name, rules=fwg_rules, ports=topo.get_ports(), enable_sg=True)
        sec_mgr.init_driver()
        sec_mgr.apply_policy()
    elif opts.firewall == 'none':
        os.system("ovs-ofctl add-flow %s 'table=60 priority=0 actions=NORMAL'" % br_name)
    
    CLI(topo.net)
    cleanup()

if __name__ == '__main__':
    main()
