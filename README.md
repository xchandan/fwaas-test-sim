The �simulator.py� is tool to test the OpenStack FWaaS and SG firewall driver. 
It uses Mininet instead of OpenStack to setup the test topology. 
Then the OVS based Firewall drivers are called to implement L2 security policy.

The simulator need to be run on your devstack VM. With the simulator, there is no need to create neutron networks or nova instances.
Access to the devstack VM is required just to ensure access to the FWaaS/SG Firewall driver code and its dependencies.

The simulator needs Mininet and NetCat to be installed on the test system.

The simulator will start a simple topology with 2 hosts connected to a OVS bridge. 
Additionally a TCP server on port 8000 is started on each simulated Host to aid TCP connectivity tests. 
The simulator can be started with �firewall option to choose which driver to use to configure firewall rules on the OVS bridge.
 You can choose between SG | FWaaS | both | none. 
The default driver is SG.

The topology should come up in few seconds and can be stopped with Ctrl+d and restarted as many times.


Usage
=====

To run it place the simulator script in the FWaaS firewall driver directory. 
/opt/stack/neutron_fwaas/services/firewall/drivers/linux/l2/openvswitch_firewall


Use the following command to start the test
sudo python simulator.py --firewall FWaaS


This will start the topology and apply FWaaS based security policy.

Demo: https://youtu.be/cuU4duzpCDg


Details: https://chandanduttachowdhury.wordpress.com/2017/11/23/using-mininet-to-test-openstack-firewall-drivers/


The Security rules for FWaaS and SG are defined in the script and is easy to modify. 
New rules can be added to test more scenarios.

e.g.
    ingress_rules = [
        {'position': '1',
         'protocol': 'icmp',
         'ip_version': 4,
         'enabled': True,
         'action': 'allow',
         'id': 'fw-rule1'},
         {'position': '2',
         'protocol': 'tcp',
         'ip_version': 4,
         'port_range_min': 8000,
         'port_range_max': 8000,
         'enabled': True,
         'action': 'allow',
         'id': 'fw-rule2'}]
 
More details about the rule structure can be found in the FWaaS/SG unit-test files.

By default ICMP allow and TCP port 8000 allow(see above TCP server on port 8000) rules are configured. 